import { useQuery } from 'react-query';
import { fetchTodoList } from '../api/todo';
import { getLocalStoreTodo, setLocalStoreTodo } from '../utils/localStorage';

export enum ServerStateKeysEnum {
  TodoItems = 'TodoItems',
}

export const useGetTodoItems = () =>
  useQuery(ServerStateKeysEnum.TodoItems, async () => {
    const items = getLocalStoreTodo();
    if (!items) {
      const fetchedItems = await fetchTodoList();
      setLocalStoreTodo(fetchedItems);
      return fetchedItems;
    }
    return items;
  });
