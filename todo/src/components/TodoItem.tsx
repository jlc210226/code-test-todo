import React from 'react';
import styles from './TodoItem.module.css';
import DeleteIcon from '@mui/icons-material/Delete';
import Checkbox from '@mui/material/Checkbox';
import { useDispatch } from 'react-redux';
import { changeTodoItemStatus, deleteTodoItem } from '../redux/todo/action';
import { changeLocalStoreTodoItem, deleteLocalStoreTodoItem } from '../utils/localStorage';
import { TodoStatus } from '../utils/variables';

interface ITodoItemProps {
  id: string;
  content: string;
  status: TodoStatus;
}

export default function TodoItem({ id, content, status }: ITodoItemProps) {
  const dispatch = useDispatch();

  const deleteHandler = (id: string) => {
    deleteLocalStoreTodoItem(id);
    dispatch(deleteTodoItem(id));
  };

  const changeStatusHandler = (id: string, currentStatus: string) => {
    const status = currentStatus === TodoStatus.Active ? TodoStatus.Completed : TodoStatus.Active;
    changeLocalStoreTodoItem(id, status);
    dispatch(changeTodoItemStatus(id, status));
  };

  return (
    <div className={styles.itemWrapper}>
      <div className={styles.todoItemView}>
        <Checkbox
          color="default"
          onClick={() => changeStatusHandler(id, status)}
          checked={status === TodoStatus.Completed}
        />
        <span className={styles.content}>{content}</span>
        <DeleteIcon onClick={() => deleteHandler(id)} />
      </div>
    </div>
  );
}
