import styles from './TodoForm.module.css';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addTodoItem } from '../redux/todo/action';
import { addLocalStoreTodoItem } from '../utils/localStorage';
import { TodoStatus } from '../utils/variables';

export default function TodoForm() {
  const dispatch = useDispatch();
  const [input, setInput] = useState('');

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (!input.trim()) {
      return;
    }
    const item = {
      id: Math.random().toString(16).slice(2),
      content: input,
      status: TodoStatus.Active,
    };
    addLocalStoreTodoItem(item);
    dispatch(addTodoItem(item));
    setInput('');
  };

  return (
    <form className={styles.todoForm} onSubmit={handleSubmit}>
      <input
        className={styles.inputField}
        type="text"
        value={input}
        onChange={(e) => setInput(e.target.value)}
        placeholder="What needs to be done?"
      />
    </form>
  );
}
