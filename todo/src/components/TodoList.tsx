import { useSelector } from 'react-redux';
import { IRootState } from '../redux/store';
import styles from './TodoList.module.css';
import Button from '@mui/material/Button';

import TodoItem from './TodoItem';
import { useState } from 'react';
import { ITodoItem } from '../redux/todo/state';
import { TodoSelectMode } from '../utils/variables';

export default function TodoList() {
  const todoItems = useSelector((state: IRootState) => state.todo.items);
  const [mode, setMode] = useState(TodoSelectMode.Active);

  const filterItemHandler = (items: ITodoItem[]) => {
    if (mode === TodoSelectMode.All) {
      return items;
    }
    return items.filter((item) => item.status === (mode as string));
  };

  const getActiveClass = (configMode: TodoSelectMode, curMode: TodoSelectMode) => {
    return configMode === curMode ? styles.active : '';
  };

  return (
    <>
      <div className={styles.statusButtonGroup}>
        <Button
          className={`${styles.statusButton} ${getActiveClass(TodoSelectMode.All, mode)}`}
          onClick={() => setMode(TodoSelectMode.All)}
        >
          All
        </Button>
        <Button
          className={`${styles.statusButton} ${getActiveClass(TodoSelectMode.Active, mode)}`}
          onClick={() => setMode(TodoSelectMode.Active)}
        >
          Active
        </Button>
        <Button
          className={`${styles.statusButton} ${getActiveClass(TodoSelectMode.Completed, mode)}`}
          onClick={() => setMode(TodoSelectMode.Completed)}
        >
          Completed
        </Button>
      </div>

      <div className={styles.listWrapper}>
        {filterItemHandler(todoItems).map((item, idx) => (
          <TodoItem {...item} key={`item-${idx}`} />
        ))}
      </div>
    </>
  );
}
