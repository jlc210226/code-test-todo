import { TodoStatus } from '../utils/variables';
import { ITodoItem } from '../redux/todo/state';

export const fetchTodoList = async () => {
  const res = await fetch('https://jsonplaceholder.typicode.com/todos');
  const data = await res.json();
  const items: ITodoItem[] = data.slice(0, 10).map((d: { id: number; title: string }) => ({
    id: d.id.toString(),
    content: d.title,
    status: TodoStatus.Active,
  }));

  return items;
};
