import React, { useEffect } from 'react';
import styles from './App.module.css';
import { useDispatch } from 'react-redux';
import { useGetTodoItems } from './hooks/todo';
import { setTodoList } from './redux/todo/action';
import { ITodoItem } from './redux/todo/state';

import TodoForm from './components/TodoForm';
import TodoList from './components/TodoList';

function App() {
  const { isLoading, data } = useGetTodoItems();
  const dispatch = useDispatch();

  useEffect(() => {
    if (!isLoading) {
      dispatch(setTodoList(data as Array<ITodoItem>));
    }
  }, [isLoading, data, dispatch]);

  return (
    <div className={styles.todoApp}>
      <div className={styles.todoTitle}>Todo List</div>
      <TodoForm />
      <TodoList />
    </div>
  );
}

export default App;
