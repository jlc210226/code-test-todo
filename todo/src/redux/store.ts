import { combineReducers, createStore } from 'redux';

import { ITodoState } from './todo/state';
import { todoReducer } from './todo/reducer';
import { ITodoActions } from './todo/action';

export interface IRootState {
  todo: ITodoState;
}

export type IRootActions = ITodoActions;

const rootReducer = combineReducers<IRootState>({
  todo: todoReducer,
});

export default createStore<IRootState, IRootActions, {}, {}>(rootReducer);
