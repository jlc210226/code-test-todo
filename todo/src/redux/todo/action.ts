import { TodoStatus } from '../../utils/variables';
import { ITodoItem } from './state';

export function setTodoList(items: Array<ITodoItem>) {
  return {
    type: '@@TODO_SET_TODO_ITEMS' as const,
    items,
  };
}

export function addTodoItem(item: ITodoItem) {
  return { type: '@@TODO_ADD_ITEM' as const, item };
}

export function deleteTodoItem(itemId: string) {
  return { type: '@@TODO_DELETE_ITEM' as const, itemId };
}

export function changeTodoItemStatus(itemId: string, status: TodoStatus) {
  return { type: '@@TODO_CHANGE_STATUS' as const, itemId, status };
}

type ActionCreators =
  | typeof setTodoList
  | typeof addTodoItem
  | typeof deleteTodoItem
  | typeof changeTodoItemStatus;
export type ITodoActions = ReturnType<ActionCreators>;
