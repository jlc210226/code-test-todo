import { ITodoState } from './state';
import { ITodoActions } from './action';
import produce from 'immer';

const initTodoState: ITodoState = {
  items: [],
};

export const todoReducer = produce((state: ITodoState, action: ITodoActions) => {
  switch (action.type) {
    case '@@TODO_SET_TODO_ITEMS':
      state.items = action.items;
      return;

    case '@@TODO_ADD_ITEM':
      state.items = [action.item, ...state.items];
      return;

    case '@@TODO_DELETE_ITEM':
      state.items = state.items.filter((item) => item.id !== action.itemId);
      return;

    case '@@TODO_CHANGE_STATUS':
      let item = state.items.find((item) => item.id === action.itemId);
      if (item) {
        item.status = action.status;
      }
      return;
  }
}, initTodoState);
