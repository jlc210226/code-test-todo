import { TodoStatus } from '../../utils/variables';

export interface ITodoItem {
  id: string;
  content: string;
  status: TodoStatus;
}

export interface ITodoState {
  items: Array<ITodoItem>;
}
