import { ITodoItem } from '../redux/todo/state';
import { TodoStatus } from './variables';

export const getLocalStoreTodo = () => {
  const items = localStorage.getItem('todo');
  if (!items) {
    return null;
  }
  return JSON.parse(items) as Array<ITodoItem>;
};

export const setLocalStoreTodo = (items: Array<ITodoItem>) => {
  localStorage.setItem('todo', JSON.stringify(items));
};

export const addLocalStoreTodoItem = (item: ITodoItem) => {
  const items = getLocalStoreTodo();
  const newItems: Array<ITodoItem> = !items ? [item] : [item, ...items];
  setLocalStoreTodo(newItems);
};

export const deleteLocalStoreTodoItem = (itemId: string) => {
  const items = getLocalStoreTodo();
  if (items) {
    const newItems = items.filter((item) => item.id !== itemId);
    setLocalStoreTodo(newItems);
  }
};

export const changeLocalStoreTodoItem = (itemId: string, status: TodoStatus) => {
  const items = getLocalStoreTodo();
  if (items) {
    let targetItem = items.find((item) => item.id === itemId);
    if (targetItem) {
      targetItem.status = status;
      setLocalStoreTodo(items);
    }
  }
};
