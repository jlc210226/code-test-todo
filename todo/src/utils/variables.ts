export enum TodoStatus {
  'Active' = 'active',
  'Completed' = 'completed',
}

export enum TodoSelectMode {
  'All' = 'all',
  'Active' = 'active',
  'Completed' = 'completed',
}
