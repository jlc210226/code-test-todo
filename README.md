# Todo List

Created by Crystal Tsang

Time Spent: 3.5 hours

&nbsp;

## Commands

Go to the project folder

```bash
cd todo

```

&nbsp;

Install the packages

```bash
yarn install

```

&nbsp;

Format the project with `prettier` (Optional)

```bash
yarn prettier

```

&nbsp;

Runs the app in the development mode. Open http://localhost:3000 to view it in the browser.

```bash
yarn start

```

&nbsp;

## Finished Functions

- [x] Add / delete items to / from the list
- [x] Fetch a list of todo items from https://jsonplaceholder.typicode.com/todos and append the first 10 items to the list when first load
- [x] The changes are kept when the app is re-opened
- [x] Bonus: Mark the item as completed with a click to the checkbox next to the item
- [x] Bonus: Categorize items as "All", "Completed", "Active"
- [x] Bonus: Style the application with your own CSS

&nbsp;

## Screen Capture

**All Tasks:**

![](images/all.png)

**Active Tasks:**

![](images/active.png)

**Completed Tasks:**

![](images/completed.png)

## Folder Structure

```bash
.
├── README.md
└── todo
    ├── package.json
    ├── public
    │   ├── favicon.ico
    │   ├── index.html
    │   ├── logo192.png
    │   ├── logo512.png
    │   ├── manifest.json
    │   └── robots.txt
    ├── src
    │   ├── App.module.css
    │   ├── App.tsx
    │   ├── api
    │   │   └── todo.ts
    │   ├── components
    │   │   ├── TodoForm.module.css
    │   │   ├── TodoForm.tsx
    │   │   ├── TodoItem.module.css
    │   │   ├── TodoItem.tsx
    │   │   ├── TodoList.module.css
    │   │   └── TodoList.tsx
    │   ├── hooks
    │   │   └── todo.ts
    │   ├── index.css
    │   ├── index.tsx
    │   ├── react-app-env.d.ts
    │   ├── redux
    │   │   ├── store.ts
    │   │   └── todo
    │   │       ├── action.ts
    │   │       ├── reducer.ts
    │   │       └── state.ts
    │   ├── reportWebVitals.ts
    │   ├── setupTests.ts
    │   └── utils
    │       ├── localStorage.ts
    │       └── variables.ts
    ├── tsconfig.json
    └── yarn.lock
```

&nbsp;

## Installed Packages

Following packages are installed:

- typescript
- react
- react-dom
- react-query
- react-redux
- redux
- mui
- immer

&nbsp;
